<?php
	
	if (isset($_POST['send']) && isset($_POST['_token'])) {

		$fullName	=	htmlentities($_POST['name']);
        $email		=	htmlentities($_POST['email']);
		$userMsg	=	htmlentities($_POST['message']);
		$subject 	=	'New Message';

		if (!empty($fullName) || !empty($email) || !empty($userMsg)) {

			/* Construct the email body */

			$message     = '<html><head><title>'.$subject.'</title></head><body><table><tr><td>Email :  </td><td> '.$email.'</td></tr><tr><td>Name : </td><td> '.$fullName.'</td></tr><tr><td>Message : </td><td> '.$userMsg.'</td></tr></table></body></html>';
			$to 		 = 'ndavidngugi@gmail.com';
			$headers 	 = "From: " . strip_tags($email) . "\r\n";
			$headers 	.= "Reply-To: ". strip_tags($email) . "\r\n";
			$headers 	.= "MIME-Version: 1.0\r\n";
			$headers 	.= "Content-Type:text/html;charset=UTF-8\r\n";

			$send = mail($to, $subject, $message, $headers);

			/* Send email */
			if(!$send){
				echo '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Sorry!</strong> Your message could not be sent!
				</div>';
	      	}else if ($send) {
	      		echo '<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Success!</strong> Your message has been sent!
				</div>';
			}
		}else{
			echo '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Error!</strong> Fill in all the required fields!
				</div>';
		}



    }

?>
