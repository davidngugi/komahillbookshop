<?php
date_default_timezone_set('Etc/UTC');

require_once './PHPMailer/PHPMailerAutoload.php';

if (isset($_POST['send']) && isset($_POST['_token'])) {

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	$mail->isSMTP();

	$fullName	=	htmlentities($_POST['name']);
    $email		=	htmlentities($_POST['email']);
	$userMsg	=	htmlentities($_POST['message']);
	$subject 	=	'New Message from KomaHill Website';

	//Set the hostname of the mail server
	$mail->Host = "";
	//Set the SMTP port number 
	$mail->Port = 465;
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = '';                 // SMTP username
	$mail->Password = '';                           // SMTP password
	//$mail->SMTPSecure = 'tls';     
	//Set who the message is to be sent from
	$mail->setFrom($email, $fullName);
	//Set an alternative reply-to address
	$mail->addReplyTo('', 'Komahill Bookshop');
	// Set Email format to HTML
	$mail->isHTML(true);    
	//Set the subject line
	$mail->Subject = $subject;

	$mail->Body = '<html><head><title>'.$subject.'</title></head><body><table><tr><td>Email id :  </td><td> '.$email.'</			td></tr><tr><td>Name : </td><td> '.$fullName.'</td></tr><tr><td>Says : </td><td> '.$userMsg.'</td></tr></table></body></html>';

	//send the message, check for errors
	if (!$mail->send()) {
	    echo '<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Sorry!</strong> Your message could not be sent!
				</div>';
	} else {
	    echo '<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Success!</strong> Your message has been sent!
				</div>';
	}
}