//This is the "Offline page" service worker
var cacheName = 'v1';
var cacheFiles =
[
    '/',
    '/index.html',
    '/manifest.json',
    '/browserconfig.xml',
    'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
    '/css/animate.css',
    '/css/bootstrap-responsive.css',
    '/css/bootstrap-responsive.min.css',
    '/css/bootstrap.css',
    '/css/bootstrap.min.css',
    '/css/flexslider.css',
    '/css/font-awesome-ie7.css',
    '/css/font-awesome.css',
    '/css/overwrite.css',
    '/css/prettyPhoto.css',
    '/css/style.css',
    '/js/animate.js',
    '/js/bootstrap.min.js',
    '/js/custom.js',
    '/js/inview.js',
    '/js/isotope.js',
    '/js/jquery-hover-effect.js',
    '/js/jquery.flexslider.js',
    '/js/jquery.js',
    '/js/jquery.localscroll-1.2.7-min.js',
    '/js/jquery.nav.js',
    '/js/jquery.prettyPhoto.js',
    '/js/jquery.scrollTo.js',
    '/js/validate.js',
    '/img/bg_direction_nav.png',
    '/img/dokimos.jpg',
    '/img/glyphicons-halflings.png',
    '/img/kids.png',
    '/img/KomaHillLogo.PNG',
    '/img/KomaHillLogoBanner.jpg',
    '/img/KomaHillLogoText.PNG',
    '/img/moed.jpg',
    '/img/neostream.png',
    '/img/ngs.png',
    '/img/omm.jpg',
    '/img/precision.jpg',
    '/img/bg/bookshelf.jpg',
    '/img/bg/background.jpg',
    '/img/bg/bg-1.jpg',
    '/img/bg/bg-2.jpg',
    '/img/bg/bg-3.jpg',
    '/img/icons/books_2.png',
    '/img/icons/books.png',
    '/img/icons/chalk.png',
    '/img/icons/furniture.png',
    '/img/icons/laptop.png',
    '/img/icons/rubber-stamp.png',
    '/img/prettyPhoto/default/default_thumb.png',
    '/img/prettyPhoto/default/loader.gif',
    '/img/prettyPhoto/default/sprite_next.png',
    '/img/prettyPhoto/default/sprite_prev.png',
    '/img/prettyPhoto/default/sprite_x.png',
    '/img/prettyPhoto/default/sprite_y.png',
    '/img/prettyPhoto/default/sprite.png',
    '/app_icons/android-chrome-192x192.png',
    '/app_icons/android-chrome-256x256.png',
    '/app_icons/apple-touch-icon.png',
    '/app_icons/favicon-16x16.png',
    '/app_icons/favicon-32x32.png',
    '/app_icons/favicon.ico',
    '/app_icons/mstile-150x150.png',
    '/app_icons/safari-pinned-tab.svg',
    '/color/default.css',
    '/font/fontawesome-webfont.eot',
    '/font/fontawesome-webfont.svg',
    '/font/fontawesome-webfont.ttf',
    '/font/fontawesome-webfont.woff',
    '/font/fontAwesome.otf'
];

//Install stage sets up the offline page in the cache and opens a new cache
self.addEventListener('install', function(event) {
  event.waitUntil(
        caches.open(cacheName).then(function(cache) {
          console.log('[CACHE STORE] Cached offline page during Install');
          return cache.addAll(cacheFiles);
        })
    );
});

self.addEventListener('activate', function(event){
    console.info( '[CACHE STORE] Service Worker activated' );

    event.waitUntil(
        caches.keys().then(function(cacheNames){
            return Promise.all(cacheNames.map(function(thisCacheName){
                if(thisCacheName !== cacheName){
                    console.log("[CACHE STORE] Removing cached files from " + thisCacheName);
                    return caches.delete(thisCacheName);
                }
            }))
        })
    );
});
//If any fetch fails, it will show the offline page.
self.addEventListener('fetch', function(event) {
  event.respondWith(
      caches.match(event.request).then(function(response){
          if(response){
              console.info('[CACHE STORE] Found in cache ', event.request.url);
              return response;
          }

          var requestClone = event.request.clone();

          fetch(requestClone).then(function(response){
              if(!response){
                  console.error("[CACHE STORE] No response from fetch");
                  return response;
              }

              var responseClone = response.clone();

              caches.open(cacheName).then(function(cache) {
                console.log('[CACHE STORE] Cached offline page during fetch');
                cache.put(event.request, responseClone);
                return response;
              })

          })
          .catch(function(err){
               console.error("[CACHE STORE] Fetch and caching failed ", err);
               return event.default();
          })
          .catch(function(){
              return catches.match('/index.html');
          })
      })

    )
});
